// Generated from BKOOL.g4 by ANTLR 4.7.2

	package bkool.parser;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKOOLLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		WS=1, BC=2, LC=3, KEYW_BOOLEAN=4, KEYW_EXTENDS=5, KEYW_FINAL=6, KEYW_CONTINUE=7, 
		KEYW_INT=8, KEYW_TRUE=9, KEYW_STATIC=10, KEYW_DO=11, KEYW_NEW=12, KEYW_FALSE=13, 
		KEYW_TO=14, KEYW_ELSE=15, KEYW_STRING=16, KEYW_VOID=17, KEYW_DOWNTO=18, 
		KEYW_THEN=19, KEYW_NIL=20, KEYW_BREAK=21, KEYW_FLOAT=22, KEYW_FOR=23, 
		KEYW_THIS=24, KEYW_CLASS=25, KEYW_IF=26, KEYW_RETURN=27, ID=28, INTERGER_LITERAL=29, 
		BOOLEAN_LITERAL=30, FLOAT_LITERAL=31, SPRT_LP=32, SPRT_RP=33, SPRT_LRQB=34, 
		SPRT_LB=35, SPRT_RB=36, SPRT_SCOL=37, SPRT_COL=38, SPRT_DOT=39, SPRT_COM=40, 
		OPRT_NOTEQUAL=41, OPRT_EQUAL=42, OPRT_LT=43, OPRT_GT=44, OPRT_LE=45, OPRT_GE=46, 
		OPRT_OR=47, OPRT_AND=48, OPRT_BANG=49, OPRT_CARET=50, OPRT_ADD=51, OPRT_SUB=52, 
		OPRT_MUL=53, OPRT_FLOAT_DIV=54, OPRT_INT_DIV=55, OPRT_MOD=56, ILLEGAL_ESCAPE=57, 
		UNCLOSE_STRING=58, STRING_LITERAL=59, UNTERMINATED_COMMENT=60, UNTERMINATED_COMMENT_INLINE=61, 
		ERROR_TOKEN=62;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"WS", "BC", "LC", "KEYW_BOOLEAN", "KEYW_EXTENDS", "KEYW_FINAL", "KEYW_CONTINUE", 
			"KEYW_INT", "KEYW_TRUE", "KEYW_STATIC", "KEYW_DO", "KEYW_NEW", "KEYW_FALSE", 
			"KEYW_TO", "KEYW_ELSE", "KEYW_STRING", "KEYW_VOID", "KEYW_DOWNTO", "KEYW_THEN", 
			"KEYW_NIL", "KEYW_BREAK", "KEYW_FLOAT", "KEYW_FOR", "KEYW_THIS", "KEYW_CLASS", 
			"KEYW_IF", "KEYW_RETURN", "ID", "INTERGER_LITERAL", "BOOLEAN_LITERAL", 
			"FLOAT_LITERAL", "SPRT_LP", "SPRT_RP", "SPRT_LRQB", "SPRT_LB", "SPRT_RB", 
			"SPRT_SCOL", "SPRT_COL", "SPRT_DOT", "SPRT_COM", "OPRT_NOTEQUAL", "OPRT_EQUAL", 
			"OPRT_LT", "OPRT_GT", "OPRT_LE", "OPRT_GE", "OPRT_OR", "OPRT_AND", "OPRT_BANG", 
			"OPRT_CARET", "OPRT_ADD", "OPRT_SUB", "OPRT_MUL", "OPRT_FLOAT_DIV", "OPRT_INT_DIV", 
			"OPRT_MOD", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", "STRING_LITERAL", "UNTERMINATED_COMMENT", 
			"UNTERMINATED_COMMENT_INLINE", "ERROR_TOKEN"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, "'boolean'", "'extends'", "'final'", "'continue'", 
			"'int'", "'true'", "'static'", "'do'", "'new'", "'false'", "'to'", "'else'", 
			"'string'", "'void'", "'downto'", "'then'", "'nil'", "'break'", "'float'", 
			"'for'", "'this'", "'class'", "'if'", "'return'", null, null, null, null, 
			"'{'", "'}'", "'|'", "'('", "')'", "';'", "':'", "'.'", "','", "'!='", 
			"'=='", "'<'", "'>'", "'<='", "'>='", "'||'", "'&&'", "'!'", "'^'", null, 
			"'-'", "'*'", "'/'", "'\\'", "'%'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "WS", "BC", "LC", "KEYW_BOOLEAN", "KEYW_EXTENDS", "KEYW_FINAL", 
			"KEYW_CONTINUE", "KEYW_INT", "KEYW_TRUE", "KEYW_STATIC", "KEYW_DO", "KEYW_NEW", 
			"KEYW_FALSE", "KEYW_TO", "KEYW_ELSE", "KEYW_STRING", "KEYW_VOID", "KEYW_DOWNTO", 
			"KEYW_THEN", "KEYW_NIL", "KEYW_BREAK", "KEYW_FLOAT", "KEYW_FOR", "KEYW_THIS", 
			"KEYW_CLASS", "KEYW_IF", "KEYW_RETURN", "ID", "INTERGER_LITERAL", "BOOLEAN_LITERAL", 
			"FLOAT_LITERAL", "SPRT_LP", "SPRT_RP", "SPRT_LRQB", "SPRT_LB", "SPRT_RB", 
			"SPRT_SCOL", "SPRT_COL", "SPRT_DOT", "SPRT_COM", "OPRT_NOTEQUAL", "OPRT_EQUAL", 
			"OPRT_LT", "OPRT_GT", "OPRT_LE", "OPRT_GE", "OPRT_OR", "OPRT_AND", "OPRT_BANG", 
			"OPRT_CARET", "OPRT_ADD", "OPRT_SUB", "OPRT_MUL", "OPRT_FLOAT_DIV", "OPRT_INT_DIV", 
			"OPRT_MOD", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", "STRING_LITERAL", "UNTERMINATED_COMMENT", 
			"UNTERMINATED_COMMENT_INLINE", "ERROR_TOKEN"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	@Override
	public Token emit() {
	    switch (getType()) {
	        case ILLEGAL_ESCAPE:
	            Token result = super.emit();
	            throw new IllegalEscape(result.getText());

	        case UNCLOSE_STRING:
	            result = super.emit();
	            throw new UncloseString(result.getText());

	        case ERROR_TOKEN:
	            result = super.emit();
	            throw new ErrorToken(result.getText());

	        case UNTERMINATED_COMMENT:
	            result = super.emit();
	            throw new UnterminatedComment();

	        case UNTERMINATED_COMMENT_INLINE:
	            result = super.emit();
	            throw new UnterminatedComment();

	        default:
	            return super.emit();
	        }
	    }


	public BKOOLLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "BKOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2@\u01cd\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\3\2\6\2\u0081\n\2\r\2\16\2\u0082\3\2\3\2\3\3\3\3\3\3\3\3"+
		"\7\3\u008b\n\3\f\3\16\3\u008e\13\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4"+
		"\7\4\u0099\n\4\f\4\16\4\u009c\13\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\13\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\31"+
		"\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\35\6\35\u0126\n\35\r\35\16\35\u0127\3"+
		"\35\7\35\u012b\n\35\f\35\16\35\u012e\13\35\3\36\3\36\3\36\7\36\u0133\n"+
		"\36\f\36\16\36\u0136\13\36\5\36\u0138\n\36\3\37\3\37\5\37\u013c\n\37\3"+
		" \3 \3 \7 \u0141\n \f \16 \u0144\13 \5 \u0146\n \3 \3 \3 \6 \u014b\n "+
		"\r \16 \u014c\5 \u014f\n \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3"+
		"\'\3(\3(\3)\3)\3*\3*\3*\3+\3+\3+\3,\3,\3-\3-\3.\3.\3.\3/\3/\3/\3\60\3"+
		"\60\3\60\3\61\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3"+
		"\66\3\67\3\67\38\38\39\39\3:\3:\3:\3:\7:\u018d\n:\f:\16:\u0190\13:\3:"+
		"\3:\3:\5:\u0195\n:\3:\3:\3:\7:\u019a\n:\f:\16:\u019d\13:\3:\5:\u01a0\n"+
		":\3;\3;\3;\3;\7;\u01a6\n;\f;\16;\u01a9\13;\3<\3<\3<\3<\7<\u01af\n<\f<"+
		"\16<\u01b2\13<\3<\3<\3=\3=\3=\3=\3=\3=\7=\u01bc\n=\f=\16=\u01bf\13=\3"+
		"=\3=\3>\3>\7>\u01c5\n>\f>\16>\u01c8\13>\3>\3>\3?\3?\3\u008c\2@\3\3\5\4"+
		"\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22"+
		"#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C"+
		"#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k\67m8o9q:s;u<w"+
		"=y>{?}@\3\2\20\5\2\13\f\17\17\"\"\4\2\f\f\17\17\5\2C\\aac|\6\2\62;C\\"+
		"aac|\3\2\63;\3\2\62;\4\2GGgg\4\2//~~\t\2$$^^ddhhppttvv\7\2\f\f$$GHQQ^"+
		"^\6\2\f\f$$GHQQ\3\2\61\61\3\2,,\3\2\'\'\2\u01e5\2\3\3\2\2\2\2\5\3\2\2"+
		"\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3"+
		"\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3"+
		"\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2"+
		"\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2"+
		"Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3"+
		"\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2"+
		"\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\3"+
		"\u0080\3\2\2\2\5\u0086\3\2\2\2\7\u0094\3\2\2\2\t\u009f\3\2\2\2\13\u00a7"+
		"\3\2\2\2\r\u00af\3\2\2\2\17\u00b5\3\2\2\2\21\u00be\3\2\2\2\23\u00c2\3"+
		"\2\2\2\25\u00c7\3\2\2\2\27\u00ce\3\2\2\2\31\u00d1\3\2\2\2\33\u00d5\3\2"+
		"\2\2\35\u00db\3\2\2\2\37\u00de\3\2\2\2!\u00e3\3\2\2\2#\u00ea\3\2\2\2%"+
		"\u00ef\3\2\2\2\'\u00f6\3\2\2\2)\u00fb\3\2\2\2+\u00ff\3\2\2\2-\u0105\3"+
		"\2\2\2/\u010b\3\2\2\2\61\u010f\3\2\2\2\63\u0114\3\2\2\2\65\u011a\3\2\2"+
		"\2\67\u011d\3\2\2\29\u0125\3\2\2\2;\u0137\3\2\2\2=\u013b\3\2\2\2?\u013d"+
		"\3\2\2\2A\u0150\3\2\2\2C\u0152\3\2\2\2E\u0154\3\2\2\2G\u0156\3\2\2\2I"+
		"\u0158\3\2\2\2K\u015a\3\2\2\2M\u015c\3\2\2\2O\u015e\3\2\2\2Q\u0160\3\2"+
		"\2\2S\u0162\3\2\2\2U\u0165\3\2\2\2W\u0168\3\2\2\2Y\u016a\3\2\2\2[\u016c"+
		"\3\2\2\2]\u016f\3\2\2\2_\u0172\3\2\2\2a\u0175\3\2\2\2c\u0178\3\2\2\2e"+
		"\u017a\3\2\2\2g\u017c\3\2\2\2i\u017e\3\2\2\2k\u0180\3\2\2\2m\u0182\3\2"+
		"\2\2o\u0184\3\2\2\2q\u0186\3\2\2\2s\u0188\3\2\2\2u\u01a1\3\2\2\2w\u01aa"+
		"\3\2\2\2y\u01b5\3\2\2\2{\u01c2\3\2\2\2}\u01cb\3\2\2\2\177\u0081\t\2\2"+
		"\2\u0080\177\3\2\2\2\u0081\u0082\3\2\2\2\u0082\u0080\3\2\2\2\u0082\u0083"+
		"\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0085\b\2\2\2\u0085\4\3\2\2\2\u0086"+
		"\u0087\7\61\2\2\u0087\u0088\7,\2\2\u0088\u008c\3\2\2\2\u0089\u008b\13"+
		"\2\2\2\u008a\u0089\3\2\2\2\u008b\u008e\3\2\2\2\u008c\u008d\3\2\2\2\u008c"+
		"\u008a\3\2\2\2\u008d\u008f\3\2\2\2\u008e\u008c\3\2\2\2\u008f\u0090\7,"+
		"\2\2\u0090\u0091\7\61\2\2\u0091\u0092\3\2\2\2\u0092\u0093\b\3\2\2\u0093"+
		"\6\3\2\2\2\u0094\u0095\7\'\2\2\u0095\u0096\7\'\2\2\u0096\u009a\3\2\2\2"+
		"\u0097\u0099\n\3\2\2\u0098\u0097\3\2\2\2\u0099\u009c\3\2\2\2\u009a\u0098"+
		"\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009d\3\2\2\2\u009c\u009a\3\2\2\2\u009d"+
		"\u009e\b\4\2\2\u009e\b\3\2\2\2\u009f\u00a0\7d\2\2\u00a0\u00a1\7q\2\2\u00a1"+
		"\u00a2\7q\2\2\u00a2\u00a3\7n\2\2\u00a3\u00a4\7g\2\2\u00a4\u00a5\7c\2\2"+
		"\u00a5\u00a6\7p\2\2\u00a6\n\3\2\2\2\u00a7\u00a8\7g\2\2\u00a8\u00a9\7z"+
		"\2\2\u00a9\u00aa\7v\2\2\u00aa\u00ab\7g\2\2\u00ab\u00ac\7p\2\2\u00ac\u00ad"+
		"\7f\2\2\u00ad\u00ae\7u\2\2\u00ae\f\3\2\2\2\u00af\u00b0\7h\2\2\u00b0\u00b1"+
		"\7k\2\2\u00b1\u00b2\7p\2\2\u00b2\u00b3\7c\2\2\u00b3\u00b4\7n\2\2\u00b4"+
		"\16\3\2\2\2\u00b5\u00b6\7e\2\2\u00b6\u00b7\7q\2\2\u00b7\u00b8\7p\2\2\u00b8"+
		"\u00b9\7v\2\2\u00b9\u00ba\7k\2\2\u00ba\u00bb\7p\2\2\u00bb\u00bc\7w\2\2"+
		"\u00bc\u00bd\7g\2\2\u00bd\20\3\2\2\2\u00be\u00bf\7k\2\2\u00bf\u00c0\7"+
		"p\2\2\u00c0\u00c1\7v\2\2\u00c1\22\3\2\2\2\u00c2\u00c3\7v\2\2\u00c3\u00c4"+
		"\7t\2\2\u00c4\u00c5\7w\2\2\u00c5\u00c6\7g\2\2\u00c6\24\3\2\2\2\u00c7\u00c8"+
		"\7u\2\2\u00c8\u00c9\7v\2\2\u00c9\u00ca\7c\2\2\u00ca\u00cb\7v\2\2\u00cb"+
		"\u00cc\7k\2\2\u00cc\u00cd\7e\2\2\u00cd\26\3\2\2\2\u00ce\u00cf\7f\2\2\u00cf"+
		"\u00d0\7q\2\2\u00d0\30\3\2\2\2\u00d1\u00d2\7p\2\2\u00d2\u00d3\7g\2\2\u00d3"+
		"\u00d4\7y\2\2\u00d4\32\3\2\2\2\u00d5\u00d6\7h\2\2\u00d6\u00d7\7c\2\2\u00d7"+
		"\u00d8\7n\2\2\u00d8\u00d9\7u\2\2\u00d9\u00da\7g\2\2\u00da\34\3\2\2\2\u00db"+
		"\u00dc\7v\2\2\u00dc\u00dd\7q\2\2\u00dd\36\3\2\2\2\u00de\u00df\7g\2\2\u00df"+
		"\u00e0\7n\2\2\u00e0\u00e1\7u\2\2\u00e1\u00e2\7g\2\2\u00e2 \3\2\2\2\u00e3"+
		"\u00e4\7u\2\2\u00e4\u00e5\7v\2\2\u00e5\u00e6\7t\2\2\u00e6\u00e7\7k\2\2"+
		"\u00e7\u00e8\7p\2\2\u00e8\u00e9\7i\2\2\u00e9\"\3\2\2\2\u00ea\u00eb\7x"+
		"\2\2\u00eb\u00ec\7q\2\2\u00ec\u00ed\7k\2\2\u00ed\u00ee\7f\2\2\u00ee$\3"+
		"\2\2\2\u00ef\u00f0\7f\2\2\u00f0\u00f1\7q\2\2\u00f1\u00f2\7y\2\2\u00f2"+
		"\u00f3\7p\2\2\u00f3\u00f4\7v\2\2\u00f4\u00f5\7q\2\2\u00f5&\3\2\2\2\u00f6"+
		"\u00f7\7v\2\2\u00f7\u00f8\7j\2\2\u00f8\u00f9\7g\2\2\u00f9\u00fa\7p\2\2"+
		"\u00fa(\3\2\2\2\u00fb\u00fc\7p\2\2\u00fc\u00fd\7k\2\2\u00fd\u00fe\7n\2"+
		"\2\u00fe*\3\2\2\2\u00ff\u0100\7d\2\2\u0100\u0101\7t\2\2\u0101\u0102\7"+
		"g\2\2\u0102\u0103\7c\2\2\u0103\u0104\7m\2\2\u0104,\3\2\2\2\u0105\u0106"+
		"\7h\2\2\u0106\u0107\7n\2\2\u0107\u0108\7q\2\2\u0108\u0109\7c\2\2\u0109"+
		"\u010a\7v\2\2\u010a.\3\2\2\2\u010b\u010c\7h\2\2\u010c\u010d\7q\2\2\u010d"+
		"\u010e\7t\2\2\u010e\60\3\2\2\2\u010f\u0110\7v\2\2\u0110\u0111\7j\2\2\u0111"+
		"\u0112\7k\2\2\u0112\u0113\7u\2\2\u0113\62\3\2\2\2\u0114\u0115\7e\2\2\u0115"+
		"\u0116\7n\2\2\u0116\u0117\7c\2\2\u0117\u0118\7u\2\2\u0118\u0119\7u\2\2"+
		"\u0119\64\3\2\2\2\u011a\u011b\7k\2\2\u011b\u011c\7h\2\2\u011c\66\3\2\2"+
		"\2\u011d\u011e\7t\2\2\u011e\u011f\7g\2\2\u011f\u0120\7v\2\2\u0120\u0121"+
		"\7w\2\2\u0121\u0122\7t\2\2\u0122\u0123\7p\2\2\u01238\3\2\2\2\u0124\u0126"+
		"\t\4\2\2\u0125\u0124\3\2\2\2\u0126\u0127\3\2\2\2\u0127\u0125\3\2\2\2\u0127"+
		"\u0128\3\2\2\2\u0128\u012c\3\2\2\2\u0129\u012b\t\5\2\2\u012a\u0129\3\2"+
		"\2\2\u012b\u012e\3\2\2\2\u012c\u012a\3\2\2\2\u012c\u012d\3\2\2\2\u012d"+
		":\3\2\2\2\u012e\u012c\3\2\2\2\u012f\u0138\7\62\2\2\u0130\u0134\t\6\2\2"+
		"\u0131\u0133\t\7\2\2\u0132\u0131\3\2\2\2\u0133\u0136\3\2\2\2\u0134\u0132"+
		"\3\2\2\2\u0134\u0135\3\2\2\2\u0135\u0138\3\2\2\2\u0136\u0134\3\2\2\2\u0137"+
		"\u012f\3\2\2\2\u0137\u0130\3\2\2\2\u0138<\3\2\2\2\u0139\u013c\5\33\16"+
		"\2\u013a\u013c\5\23\n\2\u013b\u0139\3\2\2\2\u013b\u013a\3\2\2\2\u013c"+
		">\3\2\2\2\u013d\u0145\5;\36\2\u013e\u0142\7\60\2\2\u013f\u0141\t\7\2\2"+
		"\u0140\u013f\3\2\2\2\u0141\u0144\3\2\2\2\u0142\u0140\3\2\2\2\u0142\u0143"+
		"\3\2\2\2\u0143\u0146\3\2\2\2\u0144\u0142\3\2\2\2\u0145\u013e\3\2\2\2\u0145"+
		"\u0146\3\2\2\2\u0146\u014e\3\2\2\2\u0147\u0148\t\b\2\2\u0148\u014a\t\t"+
		"\2\2\u0149\u014b\t\7\2\2\u014a\u0149\3\2\2\2\u014b\u014c\3\2\2\2\u014c"+
		"\u014a\3\2\2\2\u014c\u014d\3\2\2\2\u014d\u014f\3\2\2\2\u014e\u0147\3\2"+
		"\2\2\u014e\u014f\3\2\2\2\u014f@\3\2\2\2\u0150\u0151\7}\2\2\u0151B\3\2"+
		"\2\2\u0152\u0153\7\177\2\2\u0153D\3\2\2\2\u0154\u0155\7~\2\2\u0155F\3"+
		"\2\2\2\u0156\u0157\7*\2\2\u0157H\3\2\2\2\u0158\u0159\7+\2\2\u0159J\3\2"+
		"\2\2\u015a\u015b\7=\2\2\u015bL\3\2\2\2\u015c\u015d\7<\2\2\u015dN\3\2\2"+
		"\2\u015e\u015f\7\60\2\2\u015fP\3\2\2\2\u0160\u0161\7.\2\2\u0161R\3\2\2"+
		"\2\u0162\u0163\7#\2\2\u0163\u0164\7?\2\2\u0164T\3\2\2\2\u0165\u0166\7"+
		"?\2\2\u0166\u0167\7?\2\2\u0167V\3\2\2\2\u0168\u0169\7>\2\2\u0169X\3\2"+
		"\2\2\u016a\u016b\7@\2\2\u016bZ\3\2\2\2\u016c\u016d\7>\2\2\u016d\u016e"+
		"\7?\2\2\u016e\\\3\2\2\2\u016f\u0170\7@\2\2\u0170\u0171\7?\2\2\u0171^\3"+
		"\2\2\2\u0172\u0173\7~\2\2\u0173\u0174\7~\2\2\u0174`\3\2\2\2\u0175\u0176"+
		"\7(\2\2\u0176\u0177\7(\2\2\u0177b\3\2\2\2\u0178\u0179\7#\2\2\u0179d\3"+
		"\2\2\2\u017a\u017b\7`\2\2\u017bf\3\2\2\2\u017c\u017d\5E#\2\u017dh\3\2"+
		"\2\2\u017e\u017f\7/\2\2\u017fj\3\2\2\2\u0180\u0181\7,\2\2\u0181l\3\2\2"+
		"\2\u0182\u0183\7\61\2\2\u0183n\3\2\2\2\u0184\u0185\7^\2\2\u0185p\3\2\2"+
		"\2\u0186\u0187\7\'\2\2\u0187r\3\2\2\2\u0188\u018e\7$\2\2\u0189\u018a\7"+
		"^\2\2\u018a\u018d\t\n\2\2\u018b\u018d\n\13\2\2\u018c\u0189\3\2\2\2\u018c"+
		"\u018b\3\2\2\2\u018d\u0190\3\2\2\2\u018e\u018c\3\2\2\2\u018e\u018f\3\2"+
		"\2\2\u018f\u0191\3\2\2\2\u0190\u018e\3\2\2\2\u0191\u0194\7^\2\2\u0192"+
		"\u0195\n\n\2\2\u0193\u0195\7\2\2\3\u0194\u0192\3\2\2\2\u0194\u0193\3\2"+
		"\2\2\u0195\u019b\3\2\2\2\u0196\u0197\7^\2\2\u0197\u019a\t\n\2\2\u0198"+
		"\u019a\n\13\2\2\u0199\u0196\3\2\2\2\u0199\u0198\3\2\2\2\u019a\u019d\3"+
		"\2\2\2\u019b\u0199\3\2\2\2\u019b\u019c\3\2\2\2\u019c\u019f\3\2\2\2\u019d"+
		"\u019b\3\2\2\2\u019e\u01a0\7$\2\2\u019f\u019e\3\2\2\2\u019f\u01a0\3\2"+
		"\2\2\u01a0t\3\2\2\2\u01a1\u01a7\7$\2\2\u01a2\u01a3\7^\2\2\u01a3\u01a6"+
		"\t\n\2\2\u01a4\u01a6\n\f\2\2\u01a5\u01a2\3\2\2\2\u01a5\u01a4\3\2\2\2\u01a6"+
		"\u01a9\3\2\2\2\u01a7\u01a5\3\2\2\2\u01a7\u01a8\3\2\2\2\u01a8v\3\2\2\2"+
		"\u01a9\u01a7\3\2\2\2\u01aa\u01b0\7$\2\2\u01ab\u01ac\7^\2\2\u01ac\u01af"+
		"\t\n\2\2\u01ad\u01af\n\f\2\2\u01ae\u01ab\3\2\2\2\u01ae\u01ad\3\2\2\2\u01af"+
		"\u01b2\3\2\2\2\u01b0\u01ae\3\2\2\2\u01b0\u01b1\3\2\2\2\u01b1\u01b3\3\2"+
		"\2\2\u01b2\u01b0\3\2\2\2\u01b3\u01b4\7$\2\2\u01b4x\3\2\2\2\u01b5\u01b6"+
		"\7\61\2\2\u01b6\u01b7\7,\2\2\u01b7\u01bd\3\2\2\2\u01b8\u01b9\7,\2\2\u01b9"+
		"\u01bc\n\r\2\2\u01ba\u01bc\n\16\2\2\u01bb\u01b8\3\2\2\2\u01bb\u01ba\3"+
		"\2\2\2\u01bc\u01bf\3\2\2\2\u01bd\u01bb\3\2\2\2\u01bd\u01be\3\2\2\2\u01be"+
		"\u01c0\3\2\2\2\u01bf\u01bd\3\2\2\2\u01c0\u01c1\7\2\2\3\u01c1z\3\2\2\2"+
		"\u01c2\u01c6\7\'\2\2\u01c3\u01c5\n\17\2\2\u01c4\u01c3\3\2\2\2\u01c5\u01c8"+
		"\3\2\2\2\u01c6\u01c4\3\2\2\2\u01c6\u01c7\3\2\2\2\u01c7\u01c9\3\2\2\2\u01c8"+
		"\u01c6\3\2\2\2\u01c9\u01ca\7\2\2\3\u01ca|\3\2\2\2\u01cb\u01cc\13\2\2\2"+
		"\u01cc~\3\2\2\2\34\2\u0082\u008c\u009a\u0127\u012c\u0134\u0137\u013b\u0142"+
		"\u0145\u014c\u014e\u018c\u018e\u0194\u0199\u019b\u019f\u01a5\u01a7\u01ae"+
		"\u01b0\u01bb\u01bd\u01c6\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}