// Generated from BKOOL.g4 by ANTLR 4.7.2

	package bkool.parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKOOLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		WS=1, BC=2, LC=3, KEYW_BOOLEAN=4, KEYW_EXTENDS=5, KEYW_FINAL=6, KEYW_CONTINUE=7, 
		KEYW_INT=8, KEYW_TRUE=9, KEYW_STATIC=10, KEYW_DO=11, KEYW_NEW=12, KEYW_FALSE=13, 
		KEYW_TO=14, KEYW_ELSE=15, KEYW_STRING=16, KEYW_VOID=17, KEYW_DOWNTO=18, 
		KEYW_THEN=19, KEYW_NIL=20, KEYW_BREAK=21, KEYW_FLOAT=22, KEYW_FOR=23, 
		KEYW_THIS=24, KEYW_CLASS=25, KEYW_IF=26, KEYW_RETURN=27, ID=28, INTERGER_LITERAL=29, 
		BOOLEAN_LITERAL=30, FLOAT_LITERAL=31, SPRT_LP=32, SPRT_RP=33, SPRT_LRQB=34, 
		SPRT_LB=35, SPRT_RB=36, SPRT_SCOL=37, SPRT_COL=38, SPRT_DOT=39, SPRT_COM=40, 
		OPRT_NOTEQUAL=41, OPRT_EQUAL=42, OPRT_LT=43, OPRT_GT=44, OPRT_LE=45, OPRT_GE=46, 
		OPRT_OR=47, OPRT_AND=48, OPRT_BANG=49, OPRT_CARET=50, OPRT_ADD=51, OPRT_SUB=52, 
		OPRT_MUL=53, OPRT_FLOAT_DIV=54, OPRT_INT_DIV=55, OPRT_MOD=56, ILLEGAL_ESCAPE=57, 
		UNCLOSE_STRING=58, STRING_LITERAL=59, UNTERMINATED_COMMENT=60, UNTERMINATED_COMMENT_INLINE=61, 
		ERROR_TOKEN=62;
	public static final int
		RULE_program = 0;
	private static String[] makeRuleNames() {
		return new String[] {
			"program"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, "'boolean'", "'extends'", "'final'", "'continue'", 
			"'int'", "'true'", "'static'", "'do'", "'new'", "'false'", "'to'", "'else'", 
			"'string'", "'void'", "'downto'", "'then'", "'nil'", "'break'", "'float'", 
			"'for'", "'this'", "'class'", "'if'", "'return'", null, null, null, null, 
			"'{'", "'}'", "'|'", "'('", "')'", "';'", "':'", "'.'", "','", "'!='", 
			"'=='", "'<'", "'>'", "'<='", "'>='", "'||'", "'&&'", "'!'", "'^'", null, 
			"'-'", "'*'", "'/'", "'\\'", "'%'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "WS", "BC", "LC", "KEYW_BOOLEAN", "KEYW_EXTENDS", "KEYW_FINAL", 
			"KEYW_CONTINUE", "KEYW_INT", "KEYW_TRUE", "KEYW_STATIC", "KEYW_DO", "KEYW_NEW", 
			"KEYW_FALSE", "KEYW_TO", "KEYW_ELSE", "KEYW_STRING", "KEYW_VOID", "KEYW_DOWNTO", 
			"KEYW_THEN", "KEYW_NIL", "KEYW_BREAK", "KEYW_FLOAT", "KEYW_FOR", "KEYW_THIS", 
			"KEYW_CLASS", "KEYW_IF", "KEYW_RETURN", "ID", "INTERGER_LITERAL", "BOOLEAN_LITERAL", 
			"FLOAT_LITERAL", "SPRT_LP", "SPRT_RP", "SPRT_LRQB", "SPRT_LB", "SPRT_RB", 
			"SPRT_SCOL", "SPRT_COL", "SPRT_DOT", "SPRT_COM", "OPRT_NOTEQUAL", "OPRT_EQUAL", 
			"OPRT_LT", "OPRT_GT", "OPRT_LE", "OPRT_GE", "OPRT_OR", "OPRT_AND", "OPRT_BANG", 
			"OPRT_CARET", "OPRT_ADD", "OPRT_SUB", "OPRT_MUL", "OPRT_FLOAT_DIV", "OPRT_INT_DIV", 
			"OPRT_MOD", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", "STRING_LITERAL", "UNTERMINATED_COMMENT", 
			"UNTERMINATED_COMMENT_INLINE", "ERROR_TOKEN"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BKOOL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BKOOLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode KEYW_CLASS() { return getToken(BKOOLParser.KEYW_CLASS, 0); }
		public TerminalNode ID() { return getToken(BKOOLParser.ID, 0); }
		public TerminalNode SPRT_LP() { return getToken(BKOOLParser.SPRT_LP, 0); }
		public TerminalNode SPRT_RP() { return getToken(BKOOLParser.SPRT_RP, 0); }
		public TerminalNode EOF() { return getToken(BKOOLParser.EOF, 0); }
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BKOOLVisitor ) return ((BKOOLVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2);
			match(KEYW_CLASS);
			setState(3);
			match(ID);
			setState(4);
			match(SPRT_LP);
			setState(5);
			match(SPRT_RP);
			setState(6);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3@\13\4\2\t\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\2\2\3\2\2\2\2\t\2\4\3\2\2\2\4\5\7\33\2\2\5\6\7"+
		"\36\2\2\6\7\7\"\2\2\7\b\7#\2\2\b\t\7\2\2\3\t\3\3\2\2\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}