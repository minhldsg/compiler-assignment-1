@echo off

cd bkool

if not exist .\bin ( 
	mkdir .\bin
)

javac -d .\bin -cp %ANTLR%;%CLASSPATH%org.antlr.v4.Tool%* .\src\bkool\Main.java .\src\bkool\parser\*.java ..\grammar\target\generated-sources\antlr4\*.java  
cd..

cd grammar
javac -d ..\bkool\bin -cp %ANTLR%;%CLASSPATH%org.antlr.v4.Tool%*;..\bkool\bin;..\bkool\bin\bkool\parser .\target\generated-sources\antlr4\*.java
cd..