cd bkool
if [ ! -d ./bin ]; then 
	mkdir ./bin
fi
javac -d ./bin -cp ./bin:$ANTLR ./src/bkool/Main.java ./src/bkool/parser/*.java ../grammar/target/generated-sources/antlr4/*.java  
cd ../grammar
javac -d ../bkool/bin -cp $ANTLR:../bkool/bin:../bkool/bin/bkool/parser ./target/generated-sources/antlr4/*.java
cd ..